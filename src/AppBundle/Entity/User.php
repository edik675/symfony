<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="id_user", columns={"id_user"}), @ORM\UniqueConstraint(name="user_name", columns={"username"})}, indexes={@ORM\Index(name="id_parent", columns={"id_parent"}), @ORM\Index(name="search", columns={"search"}, flags={"fulltext"})})
 * @ORM\Entity
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_user", type="bigint", nullable=false)
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=32, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_picture", type="text", length=65535, nullable=false)
     */
    private $profilePicture;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=30, nullable=false)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="bio", type="text", length=65535, nullable=false)
     */
    private $bio;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=150, nullable=false)
     */
    private $website;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_business", type="boolean", nullable=false)
     */
    private $isBusiness;

    /**
     * @var integer
     *
     * @ORM\Column(name="media_count", type="integer", nullable=false)
     */
    private $mediaCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="follows_count", type="integer", nullable=false)
     */
    private $followsCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="followed_by_count", type="integer", nullable=false)
     */
    private $followedByCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_parent", type="bigint", nullable=false)
     */
    private $idParent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_get_followers", type="boolean", nullable=false)
     */
    private $isGetFollowers;

    /**
     * @var string
     *
     * @ORM\Column(name="search", type="text", length=65535, nullable=false)
     */
    private $search;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return User
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set profilePicture
     *
     * @param string $profilePicture
     *
     * @return User
     */
    public function setProfilePicture($profilePicture)
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    /**
     * Get profilePicture
     *
     * @return string
     */
    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return User
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set bio
     *
     * @param string $bio
     *
     * @return User
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return User
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set isBusiness
     *
     * @param boolean $isBusiness
     *
     * @return User
     */
    public function setIsBusiness($isBusiness)
    {
        $this->isBusiness = $isBusiness;

        return $this;
    }

    /**
     * Get isBusiness
     *
     * @return boolean
     */
    public function getIsBusiness()
    {
        return $this->isBusiness;
    }

    /**
     * Set mediaCount
     *
     * @param integer $mediaCount
     *
     * @return User
     */
    public function setMediaCount($mediaCount)
    {
        $this->mediaCount = $mediaCount;

        return $this;
    }

    /**
     * Get mediaCount
     *
     * @return integer
     */
    public function getMediaCount()
    {
        return $this->mediaCount;
    }

    /**
     * Set followsCount
     *
     * @param integer $followsCount
     *
     * @return User
     */
    public function setFollowsCount($followsCount)
    {
        $this->followsCount = $followsCount;

        return $this;
    }

    /**
     * Get followsCount
     *
     * @return integer
     */
    public function getFollowsCount()
    {
        return $this->followsCount;
    }

    /**
     * Set followedByCount
     *
     * @param integer $followedByCount
     *
     * @return User
     */
    public function setFollowedByCount($followedByCount)
    {
        $this->followedByCount = $followedByCount;

        return $this;
    }

    /**
     * Get followedByCount
     *
     * @return integer
     */
    public function getFollowedByCount()
    {
        return $this->followedByCount;
    }

    /**
     * Set idParent
     *
     * @param integer $idParent
     *
     * @return User
     */
    public function setIdParent($idParent)
    {
        $this->idParent = $idParent;

        return $this;
    }

    /**
     * Get idParent
     *
     * @return integer
     */
    public function getIdParent()
    {
        return $this->idParent;
    }

    /**
     * Set isGetFollowers
     *
     * @param boolean $isGetFollowers
     *
     * @return User
     */
    public function setIsGetFollowers($isGetFollowers)
    {
        $this->isGetFollowers = $isGetFollowers;

        return $this;
    }

    /**
     * Get isGetFollowers
     *
     * @return boolean
     */
    public function getIsGetFollowers()
    {
        return $this->isGetFollowers;
    }

    /**
     * Set search
     *
     * @param string $search
     *
     * @return User
     */
    public function setSearch($search)
    {
        $this->search = $search;

        return $this;
    }

    /**
     * Get search
     *
     * @return string
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
