<?php
/**
 * Created by PhpStorm.
 * User: eduard
 * Date: 11/12/17
 * Time: 10:21 PM
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;

class MainController
{

    /**
     * @Route("/", name="main")
     */
    public function showAction ()
    {
        return new Response('Hello');
    }
}