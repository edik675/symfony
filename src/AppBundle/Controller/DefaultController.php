<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use MetzWeb\Instagram\Instagram;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Extensions\Doctrine\MatchAgainst;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

//        $_instagram = $this->get('app_instagram');
//        $x = $_instagram->test();

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(User::class);

        $users_query = $repository->createQueryBuilder('iu');

        $q = $request->query->getAlnum('q', false);
        if($q) {
            $users_query
                // ->add('where', 'MATCH_AGAINST (iu.search) AGAINST (\':searchterm\' IN BOOLEAN MODE)')
                // ->setParameter('searchterm', "{$q}")

                ->where('iu.search LIKE :searchterm')
                ->setParameter('searchterm', "%{$q}%")
            ;
        }


        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $users_query->getQuery(),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('default/index.html.twig', [
            'q' =>$q,
            'users' => $pagination,
        ]);
    }
}
