-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 20, 2017 at 09:29 AM
-- Server version: 5.7.20-0ubuntu0.17.10.1
-- PHP Version: 7.1.11-1+ubuntu17.10.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insta`
--
CREATE DATABASE IF NOT EXISTS `insta` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `insta`;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `username` varchar(32) NOT NULL,
  `profile_picture` text NOT NULL,
  `full_name` varchar(30) NOT NULL,
  `bio` text NOT NULL,
  `website` varchar(150) NOT NULL,
  `is_business` tinyint(1) NOT NULL,
  `media_count` int(11) NOT NULL,
  `follows_count` int(11) NOT NULL,
  `followed_by_count` int(11) NOT NULL,
  `id_parent` bigint(20) NOT NULL,
  `is_get_followers` tinyint(1) NOT NULL,
  `search` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `id_user`, `username`, `profile_picture`, `full_name`, `bio`, `website`, `is_business`, `media_count`, `follows_count`, `followed_by_count`, `id_parent`, `is_get_followers`, `search`) VALUES
(1, 1104360683, 'stepanian.eduard', 'https://scontent.cdninstagram.com/t51.2885-19/s150x150/13549522_876410942487178_1413558353_a.jpg', 'Eduard Stepanian', 'Some quick example text to build on the card title and make up the bulk of the card\'s content. Some quick example text to build on the card title and make up the bulk of the card\'s content.', 'http://mandil.am', 0, 90, 22, 767, 0, 1, 'stepanian.eduard'),
(2, 572729317, 'mkrtchyan007', 'https://scontent.cdninstagram.com/t51.2885-19/s150x150/17439020_448108128865551_3660494768321855488_a.jpg', 'Garush Mkrtchyan', 'Some quick example text to build on the card title and make up the bulk of the card\'s content. Some quick example text to build on the card title and make up the bulk of the card\'s content.', '', 0, 5, 629, 237, 1, 0, 'garush mkrtchyan mkrtchyan007'),
(3, 2487860274, 'gopro_mandil', 'https://scontent.cdninstagram.com/t51.2885-19/s150x150/12357498_931361083626838_25999384_a.jpg', 'GoPro Mandil', 'Some quick example text to build on the card title and make up the bulk of the card\'s content.', '', 0, 5, 629, 237, 1, 0, 'goprO mandil gopro_mandil');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_user` (`id_user`),
  ADD UNIQUE KEY `user_name` (`username`),
  ADD KEY `id_parent` (`id_parent`);
ALTER TABLE `user` ADD FULLTEXT KEY `search` (`search`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
